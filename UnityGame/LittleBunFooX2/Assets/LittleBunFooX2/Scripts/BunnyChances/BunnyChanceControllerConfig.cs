﻿using System;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.BunnyChances {

    [Serializable]
    public class BunnyChanceControllerConfig {
        [SerializeField] private int startingChances;

        public int StartingChances {
            get { return startingChances; }
            set { startingChances = value; }
        }
    }
}
