﻿using System;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.UserInterace;

namespace Assets.LittleBunFooX2.Scripts.BunnyChances {
    public class BunnyChanceControllerComp : LBF2Behaviour{
        private BunnyChanceControllerConfig config;
        private int currentChances;
        private UserInterfaceController ui;

        public void Init(BunnyChanceControllerConfig chanceConfig) {
            config = chanceConfig;
            currentChances = config.StartingChances;
        }

        public void Awake() {
            ui = FindObjectOfType<UserInterfaceController>();
        }
        
        public void RemoveChance() {
            currentChances--;
            currentChances = Math.Max(0, currentChances);
        }

        public void Update() {
            ui.UpdateChanceDisplay(currentChances);
        }

        public bool HasAnyChances() {
            return currentChances >0 ;
        }

        public void Reset() {
            currentChances = config.StartingChances;
        }
    }
}
