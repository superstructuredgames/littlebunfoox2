﻿using System.Collections.Generic;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.PlayerInput {
    public class PlayerInputController : MonoBehaviour {

        private readonly Queue<Promise<Vector2>> queuedPromises = new Queue<Promise<Vector2>>();
        
        public void Update() {
            if (Input.GetMouseButtonUp(0)) {
                Vector2 position = Input.mousePosition;
                int length = queuedPromises.Count;
                for (int i = 0; i < length; i++) {
                    Promise<Vector2> prom = queuedPromises.Dequeue();
                    prom.Resolve(position);
                }
            }    
        }
	
        public Promise<Vector2> ScreenTapped() {
            Promise<Vector2> promise = new Promise<Vector2>();
            queuedPromises.Enqueue(promise);        
            return promise;
        }
    }
}
