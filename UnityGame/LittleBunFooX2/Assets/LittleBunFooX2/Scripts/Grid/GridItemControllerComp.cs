﻿using System.Collections.Generic;
using System.Linq;
using Assets.LittleBunFooX2.Scripts.Grid;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems {
    
    public class GridItemControllerComp : LBF2Behaviour {

        private GridItemControllerConfig config;
        private GridControllerComp grid;
        private readonly List<GridItemComp> items = new List<GridItemComp>();
        private readonly IComparer<GridItemComp> prioritySort = new GridItemPrioritySort();
        private Dictionary<GridItemType, GridItemControllerEntry> configMap;
        private static int stepCount;

        private class GridItemPrioritySort : IComparer<GridItemComp> {
            public int Compare(GridItemComp x, GridItemComp y) {
                if (x == null && y == null) {
                    return 0;
                }
                if (x == null) {
                    return -1;
                }
                if (y == null) {
                    return 1;
                }
                return x.ItemPriority - y.ItemPriority;
            }
        }

        public void Awake() {
            grid = FindObjectOfType<GridControllerComp>();
        }

        public void Init(GridItemControllerConfig gridItemConfig) {
            config = gridItemConfig;
            configMap = config.GetAsMap();
        }

        public IPromise Spawn<T>(GridSpace position, GridItemType itemType, out T comp) where T : GridItemComp{
            GameObject itemObj = Instantiate(configMap[itemType].Prefab);
            itemObj.transform.parent = transform;
            comp = itemObj.GetComponent<T>();
            items.Add(comp);
            items.Sort(prioritySort);
            return comp.Spawn(grid, position);
        }

        public IPromise DespawnClean(GridItemComp item) {
            return Despawn(item, true);
        }

        public IPromise Despawn(GridItemComp item) {
            return Despawn(item, false);
        }

        private IPromise Despawn(GridItemComp item, bool isCleanUp) {
            items.Remove(item);
            return item.Despawn(isCleanUp).Then(() => {
                Destroy(item.GameObject);
            });
        }

        public IPromise StepForward() {
            Queue<List<GridItemComp>> itemsByOrder = new Queue<List<GridItemComp>>();
            List<GridItemComp> currentList = null;
            int currentPriority = int.MinValue;
            for (int i = 0; i < items.Count; i++) {
                if (items[i].ItemPriority != currentPriority || currentList == null) {
                    currentList = new List<GridItemComp>();
                    currentPriority = items[i].ItemPriority;
                    itemsByOrder.Enqueue(currentList);
                }
                currentList.Add(items[i]);
            }
            return RunOrderedChain(itemsByOrder).Then(() => {
                stepCount++;
                Debug.Log("stepCount: " + stepCount);
            });
        }

        private IPromise RunOrderedChain(Queue<List<GridItemComp>> itemsByOrder) {
            IPromise chained;
            if (itemsByOrder.Any()) {
                List<GridItemComp> orderItems = itemsByOrder.Dequeue();
                List<IPromise> promises = RunPossibleBlocks(orderItems, items);
                chained = Promise.All(promises).Then(() => RunOrderedChain(itemsByOrder));
            } else {
                chained = Promise.Resolved();
            }
            return chained;
        }

        private static List<IPromise> RunPossibleBlocks(List<GridItemComp> possibleBlockComps, List<GridItemComp> allitems) {
            List<IPromise> promises = new List<IPromise>();
            List<GridItemComp> blockedComps = new List<GridItemComp>();
            for (int i = 0; i < possibleBlockComps.Count; i++) {
                if (allitems.Contains(possibleBlockComps[i])) {
                    bool isBlocked;
                    IPromise promise = possibleBlockComps[i].StepForward(out isBlocked, stepCount);
                    if (isBlocked) {
                        blockedComps.Add(possibleBlockComps[i]);
                    }
                    promises.Add(promise);
                }
            }
            if (blockedComps.Count > 0 && blockedComps.Count != possibleBlockComps.Count) {
                promises.AddRange(RunPossibleBlocks(blockedComps, allitems));
            }
            return promises;
        }
    

        public int GetLiveCount(GridItemType type) {
            int count = 0;
            foreach (GridItemComp item in items) {
                if (item.ItemType == type) {
                    count++;
                }
            }
            return count;
        }

        public void RemoveAll() {
            while (items.Any()) {
                GridItemComp item = items[0];
                GridCellComp current = item.GetCurrent();
                if (current != null) {
                    current.RemoveOccupant(item);
                }
                Despawn(item, true);    
            }

        }
    }
}
