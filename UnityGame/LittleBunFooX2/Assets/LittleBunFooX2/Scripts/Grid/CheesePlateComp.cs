﻿using Assets.LittleBunFooX2.Scripts.GridItems;
using RSG;

namespace Assets.LittleBunFooX2.Scripts.Grid {
    public class CheesePlateComp : BaseGridItemComp {
        private GridItemPlacementManager placementManager;

        public override IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            return placementManager.Spawn(spawnGrid, spawnPosition);
        }

        public override IPromise Despawn(bool isCleanUp) {
            return Promise.Resolved();
        }

        public override IPromise StepForward(out bool isBlocked, int stepCount) {
            isBlocked = false;
            return Promise.Resolved();
        }

        public override GridCellComp GetCurrent() {
            return placementManager.GetCell(new GridSpace(0, 0));
        }

        protected override void AwakeExtended() {
            placementManager = new GridItemPlacementManager(this, this);
        }
    }
}
