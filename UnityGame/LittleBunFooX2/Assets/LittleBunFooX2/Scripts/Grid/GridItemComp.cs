﻿using Assets.LittleBunFooX2.Scripts.GridItems;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.Grid {
    public interface GridItemComp {
        IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition);
        IPromise Despawn(bool isCleanUp);
        IPromise StepForward(out bool isBlocked, int currentStep);
        GridItemType ItemType { get; }
        int ItemPriority { get; }
        GameObject GameObject { get; }
        float SpawnSpeed { get; set; }
        GridCellComp GetCurrent();
    }
}
