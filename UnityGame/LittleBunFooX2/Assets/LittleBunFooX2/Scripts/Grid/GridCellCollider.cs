﻿using Assets.LittleBunFooX2.Scripts.Grid;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems {
    public class GridCellCollider : LBF2Behaviour {

        [SerializeField] private GridCellComp cell;

        public GridCellComp Cell {
            get { return cell; }
            set { cell = value; }
        }
    }
}
