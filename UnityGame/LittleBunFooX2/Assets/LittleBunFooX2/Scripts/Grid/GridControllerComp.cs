﻿using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.GridItems;
using Assets.LittleBunFooX2.Scripts.PlayerInput;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.Grid {
    public class GridControllerComp : LBF2Behaviour {

        private static readonly RaycastHit[] RESULTS = new RaycastHit[100];
        private readonly Dictionary<GridSpace, GridCellComp> cells = new Dictionary<GridSpace, GridCellComp>(new GridSpaceComparator());
        private PlayerInputController inputController;
        private GridSpace gridSize;

        private class GridSpaceComparator : IEqualityComparer<GridSpace> {
            public bool Equals(GridSpace x, GridSpace y) {
                return x.X == y.X && x.Y == y.Y;
            }

            public int GetHashCode(GridSpace obj) {
                return obj.GetHashCode();
            }
        }

        
        public void Awake() {
            inputController = FindObjectOfType<PlayerInputController>();
        }

        public void Generate (GridControllerConfig config) {
            cells.Clear();
            gridSize = config.Size;
            for (int x = 0; x < gridSize.X; x++) {
                for (int y = 0; y < gridSize.Y; y++) {
                    GameObject cellObject = Instantiate(config.CellPrefab);
                    GridSpace gridPositon = new GridSpace(x, y);
                    GridCellComp cellComp = cellObject.GetComponent<GridCellComp>();
                    bool isAlternate =  (x + y) % 2  == 1;
                    cellComp.Place(gridSize, gridPositon, transform, isAlternate);
                    cells[gridPositon] = cellComp;
                    cellComp.GridPosition = gridPositon;
                }
            }
        }

        public GridCellComp GetCell(GridSpace position) {
            GridCellComp cell;
            cells.TryGetValue(position, out cell);
            return cell;
        }

        public Promise<GridCellComp> PlayerTapsCell() {
            Promise<GridCellComp> promise = new Promise<GridCellComp>();
            PromptInput(promise);
            return promise;
        }
        
        private void PromptInput(Promise<GridCellComp> toResolve) {
            inputController.ScreenTapped().Then(position => {
                Ray ray = Camera.main.ScreenPointToRay(position);
                int hitCount = Physics.RaycastNonAlloc(ray, RESULTS);
                GridCellComp gridCell = null;
                for (int i = 0; i < hitCount; i++) {
                    GridCellCollider gridCellCollider = RESULTS[i].collider.GetComponent<GridCellCollider>();
                    if (gridCellCollider != null) {
                        gridCell = gridCellCollider.Cell;
                        break;
                    }
                }
                if (gridCell != null) {
                    toResolve.Resolve(gridCell);
                }
                else {
                    PromptInput(toResolve);
                }
            });
        }

        public GridSpace GetGridSize() {
            return gridSize;
        }
    }
}
