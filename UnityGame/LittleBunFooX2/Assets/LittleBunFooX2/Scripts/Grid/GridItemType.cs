﻿namespace Assets.LittleBunFooX2.Scripts.GridItems
{
    public enum GridItemType{
        FieldMouse,
        Bunny,
        Hole,
        CheeseyBitz,
        CheeseyPlate
    }
}
