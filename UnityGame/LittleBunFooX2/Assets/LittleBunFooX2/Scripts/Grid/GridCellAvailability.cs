﻿using System;
using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.Grid;

namespace Assets.LittleBunFooX2.Scripts.GridItems {
    public class GridCellAvailability {

        public static List<GridSpace> GetFreeCells(GridControllerComp grid, int minCol, int maxCol, GridItemType[] ignore) {
            List<GridSpace> freeCells = new List<GridSpace>();
            GridSpace gridSize = grid.GetGridSize();
            int minX = minCol;
            int maxX = maxCol;
            minX = Math.Max(0, minX);
            for (int x = minX; x <= maxX; x++) {
                for (int y = 0; y < gridSize.Y; y++) {
                    GridSpace cellPosition = new GridSpace(x, y);
                    if (!grid.GetCell(cellPosition).HasAnyOccupant(ignore)) {
                        freeCells.Add(cellPosition);
                    }
                }
            }
            return freeCells;
        }
    }
}
