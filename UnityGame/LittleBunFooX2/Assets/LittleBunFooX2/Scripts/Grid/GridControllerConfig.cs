﻿using System;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.Grid {


    [Serializable]
    public struct GridSpace : IEquatable<GridSpace> {
        [SerializeField] private int x;
        [SerializeField] private int y;
        
        public GridSpace(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X {
            get { return x; }
            set { x = value; }
        }

        public int Y {
            get { return y; }
            set { y = value; }
        }

        public Vector2 ToVector2() {
            return new Vector2(x, y);
        }

        public override string ToString() {
            return string.Format("[{0}:{1}]", x, y);
        }

        public GridSpace Add(GridSpace other) {
            return new GridSpace(x + other.x, y + other.y);
        }

        public GridSpace Subtract(GridSpace other) {
            return new GridSpace(x - other.x, y - other.y);
        }
        
        public bool Equals(GridSpace other) {
            return x == other.x && y == other.y;
        }

        public override int GetHashCode() {
            unchecked {
                return (x*397) ^ y;
            }
        }
    }

    [Serializable]
    public class GridControllerConfig {
        [SerializeField] private GridSpace size;
        [SerializeField] private GameObject cellPrefab;

        public GridSpace Size {
            get { return size; }
            set { size = value; }
        }

        public GameObject CellPrefab {
            get { return cellPrefab; }
            set { cellPrefab = value; }
        }
    }
}
