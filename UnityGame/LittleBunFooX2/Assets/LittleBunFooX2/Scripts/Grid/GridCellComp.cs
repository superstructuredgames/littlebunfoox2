﻿using System.Collections.Generic;
using System.Linq;
using Assets.LittleBunFooX2.Scripts.GridItems;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.LittleBunFooX2.Scripts.Grid {
    public class GridCellComp : LBF2Behaviour {

        [SerializeField] private Animator animator;
        [SerializeField] private Transform corner;
        [SerializeField] private Transform oppositeCorner;
        [SerializeField] private Transform anchor;
        [SerializeField] private Text debugText;
        [SerializeField] private GameObject primary;
        [SerializeField] private GameObject alternate;

        public void Update() {
            DebugText.text = occupants.Count.ToString();
        }
        
        private readonly List<GridItemComp> occupants = new List<GridItemComp>();
        
        public Transform OppositeCorner {
            get { return oppositeCorner; }
            set { oppositeCorner = value; }
        }

        public Transform Corner {
            get { return corner; }
            set { corner = value; }
        }

        public GridSpace GridPosition { get; set; }

        public Transform Anchor {
            get { return anchor; }
            set { anchor = value; }
        }

        public Text DebugText {
            get { return debugText; }
            set { debugText = value; }
        }

        public Animator Animator1 {
            get { return animator; }
            set { animator = value; }
        }

        public GameObject Primary {
            get { return primary; }
            set { primary = value; }
        }

        public GameObject Alternate {
            get { return alternate; }
            set { alternate = value; }
        }

        private Vector2 GetCellSize() {
            Vector2 max = Vector2.Max(corner.position, oppositeCorner.position);
            return max - GetMinOffset();
        }

        public void Place(GridSpace gridSize, GridSpace position, Transform parent, bool isAlternate) {
            transform.parent = parent;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;

            Vector2 cellSize = GetCellSize();
            Vector2 minOffset = GetMinOffset();
            Vector2 positionOffset = position.ToVector2();
            positionOffset.Scale(cellSize);
            Vector2 cellPosition = gridSize.ToVector2();
            cellPosition.Scale(cellSize);

            cellPosition = (Vector2)transform.position - (cellPosition * .5f);
            cellPosition = cellPosition + positionOffset;
            transform.position = cellPosition - minOffset + ((Vector2)parent.position);
            
            alternate.SetActive(isAlternate);
            primary.SetActive(!isAlternate);
        }

        private Vector2 GetMinOffset() {
            return Vector2.Min(corner.position, oppositeCorner.position);
        }

        public Vector3 GetAnchorPoint() {
            return Anchor.transform.position;
        }

        public void AddOcccupant(GridItemComp occupant) {
            occupants.Add(occupant);
        }

        public bool ContainsOccupant(GridItemType type) {
            return GetOccupant(type) != null;
        }

        public void RemoveOccupant(GridItemComp occupant) {
            occupants.Remove(occupant);
        }

        public bool HasAnyOccupant() {
            return HasAnyOccupant(new GridItemType[0]);
        }

        public bool HasAnyOccupant(GridItemType[] ignore) {
            foreach (GridItemComp occupant in occupants) {
                if(!ignore.Contains(occupant.ItemType)) {
                    return true;
                }
            }
            return false;
        }

        public GridItemComp GetOccupant(GridItemType type) {
            return GetOccupant<GridItemComp>(type);
        }

        public T GetOccupant<T>(GridItemType type) where T : GridItemComp {
            GridItemComp found = null;
            foreach (GridItemComp occupant in occupants) {
                if (occupant.ItemType == type) {
                    found = occupant;
                    break;
                }
            }
            return (T)found;
        }

        public void Hilight() {
            animator.SetBool("Hilight", true);
        }

        public void UnHilight() {
            animator.SetBool("Hilight", false);
        }

        public void AltHilight() {
            animator.SetBool("AlternateHilight", true);
        }

        public void AltUnHilight() {
            animator.SetBool("AlternateHilight", false);
        }

        public void Dim() {
            animator.SetBool("Dim", true);
        }
    }
}
