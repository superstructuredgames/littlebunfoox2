﻿using System.Collections.Generic;
using System.Security.Cryptography;
using Assets.LittleBunFooX2.Scripts.Grid;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems{
    internal class GridItemPlacementManager {
        
        private readonly Transform itemTransform;
        private readonly GridItemComp item;
        private readonly LBF2Behaviour comp;

        private GridControllerComp grid;
        private GridCellComp current;
        
        public GridItemPlacementManager(GridItemComp item, LBF2Behaviour comp) {
            itemTransform = comp.transform;
            this.item = item;
            this.comp = comp;
        }

        public IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            GridCellComp cell = spawnGrid.GetCell(spawnPosition);
            IPromise promise;
            if (cell != null){
                grid = spawnGrid;
                itemTransform.localScale = Vector3.one;
                itemTransform.localRotation = Quaternion.identity;
                itemTransform.position = cell.GetAnchorPoint();
                current = cell;
                current.AddOcccupant(item);
                Promise prom = new Promise();
                comp.StartCoroutine(Wait(prom, item.SpawnSpeed));
                promise = prom;

            }
            else{
                Debug.Log("Failed to spawn at unknown cell'" + spawnPosition + '\'');
                promise = Promise.Resolved();
            }
            return promise;
        }

        public GridCellComp GetCell(GridSpace direction) {
            GridSpace nextPosition = current.GridPosition.Add(direction);
            return grid.GetCell(nextPosition);
        }

        public IPromise StepForward(GridSpace direction, AnimationCurve curve, out bool isBlocked) {
            return MoveTo(GetCell(direction), curve, out isBlocked);
        }

        public IPromise MoveTo(GridCellComp target, AnimationCurve curve, out bool isBlocked) {
            IPromise movePromise;
            isBlocked = true;
            if (target != null) {
                if (current != null) {
                    current.RemoveOccupant(item);
                }
                current = target;
                current.AddOcccupant(item);
                Promise aMovePromise = new Promise();
                comp.StartCoroutine(MoveTowards(current.GetAnchorPoint(), aMovePromise, curve));
                movePromise = aMovePromise;
                isBlocked = false;
            } else {
                movePromise = Promise.Resolved();
            }
            return movePromise;
        }

        
        public IEnumerator<YieldInstruction> Wait(IPendingPromise spawnPromise, float duration) {
            float startTime = Time.time;
            while ((Time.time - startTime) < duration) {
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();
            spawnPromise.Resolve();
        }

        private IEnumerator<YieldInstruction> MoveTowards(Vector3 destination, IPendingPromise movePromise, AnimationCurve curve) {
            float speed = curve.keys[curve.keys.Length - 1].time;
            float startTime = Time.time;
            Vector3 startPosition = itemTransform.position;
            while ((Time.time - startTime) < speed) {
                float value = curve.Evaluate(Time.time - startTime);
                itemTransform.position = Vector3.Lerp(startPosition, destination, value);
                yield return new WaitForEndOfFrame();
            }
            itemTransform.position = destination;
            yield return new WaitForEndOfFrame();
            movePromise.Resolve();
        }

        public IPromise Despawn(float delay) {
            Promise spawnPromise = new Promise();
            comp.StartCoroutine(Wait(spawnPromise, delay));
            return spawnPromise;

        }
    }
}
