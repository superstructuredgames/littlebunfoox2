﻿using Assets.LittleBunFooX2.Scripts.Grid;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems {
    public abstract class BaseGridItemComp : LBF2Behaviour, GridItemComp {

        [SerializeField] private GridItemType itemType;
        [SerializeField] private int itemPriority;
        [SerializeField] private float spawnSpeed;
        [SerializeField] private GameObject viewPrefab;
        [SerializeField] private Transform viewParent;
        [SerializeField] private AnimationCurve moveCurve;
        
        protected Animator animator;

        public abstract IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition);
        public abstract IPromise Despawn(bool isCleanUp);
        public abstract IPromise StepForward(out bool isBlocked, int currentStep);

        public void Awake() {
            GameObject view = Instantiate(viewPrefab);
            view.transform.parent = viewParent;
            view.transform.localScale = Vector3.one;
            view.transform.localRotation = Quaternion.identity;
            view.transform.localPosition = Vector3.zero;
            animator = view.GetComponentInChildren<Animator>();
            AwakeExtended();
        }

        protected abstract void AwakeExtended();

        public GridItemType ItemType {
            get { return itemType; }
            set { itemType = value; }
        }

        public int ItemPriority {
            get { return itemPriority; }
            set { itemPriority = value; }
        }

        public GameObject GameObject {
            get { return gameObject; }
        }
        
        public float SpawnSpeed {
            get { return spawnSpeed; }
            set { spawnSpeed = value; }
        }

        public GameObject ViewPrefab {
            get { return viewPrefab; }
            set { viewPrefab = value; }
        }

        public Transform ViewParent {
            get { return viewParent; }
            set { viewParent = value; }
        }

        public AnimationCurve MoveCurve {
            get { return moveCurve; }
            set { moveCurve = value; }
        }

        public abstract GridCellComp GetCurrent();
    }
}
