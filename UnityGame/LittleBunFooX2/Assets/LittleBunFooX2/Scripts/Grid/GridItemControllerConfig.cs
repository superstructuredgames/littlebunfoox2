﻿using System;
using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.Grid;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems {

    [Serializable]
    public class GridItemControllerConfig {

        [SerializeField] private GridItemControllerEntry[] entries;

        public GridItemControllerEntry[] Entries {
            get { return entries; }
            set { entries = value; }
        }

        public Dictionary<GridItemType, GridItemControllerEntry> GetAsMap() {
            Dictionary<GridItemType, GridItemControllerEntry> map = new Dictionary<GridItemType, GridItemControllerEntry>();
            foreach (GridItemControllerEntry entry in entries) {
                map[entry.Prefab.GetComponent<GridItemComp>().ItemType] = entry;
            }
            return map;
        }
    }

    [Serializable]
    public class GridItemControllerEntry {
        [SerializeField] private string displayName;
        [SerializeField] private GameObject prefab;
        

        public GameObject Prefab {
            get { return prefab; }
            set { prefab = value; }
        }
        
        public string DisplayName {
            get { return displayName; }
            set { displayName = value; }
        }
    }
}
