﻿using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.Audio;
using Assets.LittleBunFooX2.Scripts.CheeseStuff;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using Assets.LittleBunFooX2.Scripts.UserInterace;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.BunnyControl
{
    public class BunnyComp : BaseGridItemComp {

        [SerializeField] private AnimationCurve smashCurve;
        [SerializeField] private BunnyMoveOptionType moveType;
        [SerializeField] private AudioClip hopSound;
        [SerializeField] private AudioClip cheeseSound;

        private GridItemPlacementManager placementManger;
        private GridControllerComp grid;
        private GridCellComp target;
        private GridSpace gridPosition;
        private GridItemControllerComp itemController;
        private int boopCount;
        private BunnyMoveProvider moveProvider;
        private int rightLimit;
        private UserInterfaceController userInterface;
        private CheeseControllerComp cheeseController;
        private AudioController audioController;

        public BunnyMoveOptionType MoveType {
            get { return moveType; }
            set { moveType = value; }
        }

        public AnimationCurve SmashCurve {
            get { return smashCurve; }
            set { smashCurve = value; }
        }

        public AudioClip CheeseSound {
            get { return cheeseSound; }
            set { cheeseSound = value; }
        }

        public AudioClip HopSound {
            get { return hopSound; }
            set { hopSound = value; }
        }


        protected override void AwakeExtended() {
            placementManger = new GridItemPlacementManager(this, this);
            itemController = FindObjectOfType<GridItemControllerComp>();
            userInterface = FindObjectOfType<UserInterfaceController>();
            cheeseController = FindObjectOfType<CheeseControllerComp>();
            audioController = FindObjectOfType<AudioController>();
        }

        public void Init(BunnyMoveProvider provider, int maxRight) {
            moveProvider = provider;
            rightLimit = maxRight;
            for (int x = maxRight; x < grid.GetGridSize().X; x++) {
                for (int y = 0; y < grid.GetGridSize().Y; y++) {
                    GridSpace position = new GridSpace(x, y);
                    grid.GetCell(position).Dim();
                }
            }
        }

        private void UpdateUi() {
            userInterface.UpdateBopDisplay(boopCount);
        }

        public override IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            grid = spawnGrid;
            gridPosition = spawnPosition;
            UpdateUi();
            return placementManger.Spawn(spawnGrid, spawnPosition);    
        }

        public override IPromise Despawn(bool isCleanUp) {
            return placementManger.Despawn(0);
        }

        public override IPromise StepForward(out bool isBlocked, int currentStep) {
            gridPosition = target.GridPosition;
            GridItemComp mouse = target.GetOccupant(GridItemType.FieldMouse);
            GridItemComp cheese = target.GetOccupant(GridItemType.CheeseyBitz);
            if (mouse != null) {
                target.RemoveOccupant(mouse);
                IPromise despanPromise = itemController.Despawn(mouse);
                boopCount++;
                animator.SetTrigger("Smash");
                IPromise movePromise = placementManger.MoveTo(target, SmashCurve, out isBlocked);
                UpdateUi();
                return Promise.All(despanPromise, movePromise);

            }
            if (cheese != null) {
                target.RemoveOccupant(cheese);
                IPromise despanPromise = itemController.Despawn(cheese);
                audioController.PlayClip(CheeseSound);
                animator.SetTrigger("Walk");

                IPromise movePromise = placementManger.MoveTo(target, MoveCurve, out isBlocked);
                cheeseController.RegisterCheeseCollected();
                return Promise.All(despanPromise, movePromise);
            }
			audioController.PlayClip(HopSound);
            animator.SetTrigger("Walk");
            return placementManger.MoveTo(target, MoveCurve, out isBlocked);
        }

        public override GridCellComp GetCurrent() {
            return placementManger.GetCell(new GridSpace(0, 0));
        }

        public IPromise PlayerMoves() {
            Promise movementPromise = new Promise();
            RunMoveLoop(movementPromise);
            return movementPromise;
        }

        private void RunMoveLoop(IPendingPromise toResolve) {
            List<GridCellComp> allMoveOptions = moveProvider.GetMoveOptions(grid, gridPosition);
            List<GridCellComp> possibleMoveOptions = new List<GridCellComp>();
            foreach (GridCellComp moveOption in allMoveOptions) {
                if (moveOption.GridPosition.X < rightLimit) {
                    moveOption.Hilight();
                    possibleMoveOptions.Add(moveOption);
                }
                
            }
            grid.PlayerTapsCell().Then(cell => {
                if (possibleMoveOptions.Contains(cell)) {
                    target = cell;
                    foreach (GridCellComp possibleCell in possibleMoveOptions) {
                        possibleCell.UnHilight();
                    }
                    toResolve.Resolve();
                } else {
                    Debug.Log("nope");
                    RunMoveLoop(toResolve);
                }
            }).Catch(toResolve.Reject);
        }

        public int GetBoopCount() {
            return boopCount;
        }
    }
}
