﻿using System;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.BunnyControl {

    [Serializable]
    public class BunnyControllerConfig {
        [SerializeField] private BunnyMoveOptionType movement;
        [SerializeField] private int maxRightMove;

        public BunnyMoveOptionType Movement {
            get { return movement; }
            set { movement = value; }
        }

        public int MaxRightMove {
            get { return maxRightMove; }
            set { maxRightMove = value; }
        }
    }
}
