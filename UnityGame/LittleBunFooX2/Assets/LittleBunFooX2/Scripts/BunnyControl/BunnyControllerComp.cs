﻿using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using Assets.LittleBunFooX2.Scripts.UserInterace;
using RSG;

namespace Assets.LittleBunFooX2.Scripts.BunnyControl {

    public class BunnyControllerComp : LBF2Behaviour{
        private GridControllerComp grid;
        private GridItemControllerComp itemController;
        private BunnyComp bunny;
        private BunnyControllerConfig config;
        
        public void Awake() {
            grid = FindObjectOfType<GridControllerComp>();
            itemController = FindObjectOfType<GridItemControllerComp>();
        }

        public void Init(BunnyControllerConfig bunnyConfig) {
            config = bunnyConfig;
        }

        public IPromise SpawnBunny() {
            int height = grid.GetGridSize().Y;
            IPromise spawnPromise = itemController.Spawn(new GridSpace(0, height/2), GridItemType.Bunny, out bunny);
            bunny.Init(BunnyMoveProviderLookup.GetProvider(config.Movement), config.MaxRightMove);
            return spawnPromise;
        }

        public int GetBoopCount() {
            return bunny.GetBoopCount();
        }

        public IPromise PlayerMoves() {
            return bunny.PlayerMoves();
        }


        
    }

    
}
