﻿using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;

namespace Assets.LittleBunFooX2.Scripts.BunnyControl
{
    public interface BunnyMoveProvider {
        List<GridCellComp> GetMoveOptions(GridControllerComp grid, GridSpace current);

    }

    public class BunnyMoveProviderLookup {
        private static Dictionary<BunnyMoveOptionType, BunnyMoveProvider> providerMap = new Dictionary<BunnyMoveOptionType, BunnyMoveProvider> {
            {BunnyMoveOptionType.Diagnal, StaticMoveProvider.Diagnal},
            {BunnyMoveOptionType.Straight, StaticMoveProvider.Straight},
            {BunnyMoveOptionType.Adjacent, StaticMoveProvider.Adjacent},
            {BunnyMoveOptionType.Double , SearchMoveProvider.Dist2}
        };

        public static BunnyMoveProvider GetProvider(BunnyMoveOptionType type) {
            return providerMap[type];
        }
    }

    public class SearchMoveProvider : BunnyMoveProvider {

        public static readonly BunnyMoveProvider Dist2 = new SearchMoveProvider(2);

        private readonly int distance;

        private SearchMoveProvider(int distance) {
            this.distance = distance;
        }
        public List<GridCellComp> GetMoveOptions(GridControllerComp grid, GridSpace current) {
            List<GridCellComp> found = new List<GridCellComp>();
            RunSearchOption(grid, current, current, found, 0);
            return found;
        }

        private void RunSearchOption(GridControllerComp grid, GridSpace toCheck, GridSpace toSkip, List<GridCellComp> found, int currentDistance) {
            GridCellComp cell = grid.GetCell(toCheck);
            if (cell != null && !cell.ContainsOccupant(GridItemType.Hole)){
                if (!toSkip.Equals(cell.GridPosition) && !found.Contains(cell)) {
                    found.Add(cell);
                    if (!found.Contains(cell)) { }
                }
                if (currentDistance < distance && !cell.ContainsOccupant(GridItemType.FieldMouse)) {
                    RunSearchOption(grid, toCheck.Add(new GridSpace(1, 0)), toSkip, found, currentDistance+1);
                    RunSearchOption(grid, toCheck.Add(new GridSpace(0, 1)), toSkip, found, currentDistance + 1);
                    RunSearchOption(grid, toCheck.Add(new GridSpace(-1, 0)), toSkip, found, currentDistance + 1);
                    RunSearchOption(grid, toCheck.Add(new GridSpace(0, -1)), toSkip, found, currentDistance + 1);
                }
                    
            }
        }

    }

    public class StaticMoveProvider : BunnyMoveProvider {

        public static readonly BunnyMoveProvider Straight = new StaticMoveProvider(new[] {new GridSpace(1,0), new GridSpace(0,1), new GridSpace(-1,0), new GridSpace(0,-1)});
        public static readonly BunnyMoveProvider Diagnal = new StaticMoveProvider(new[] { new GridSpace(1, 1), new GridSpace(-1, 1), new GridSpace(-1, -1), new GridSpace(1, -1) });
        public static readonly BunnyMoveProvider Adjacent = new StaticMoveProvider(new[] { new GridSpace(1, 1), new GridSpace(-1, 1), new GridSpace(-1, -1), new GridSpace(1, -1), new GridSpace(1, 0), new GridSpace(0, 1), new GridSpace(-1, 0), new GridSpace(0, -1) });

        private readonly GridSpace[] options;

        private StaticMoveProvider(GridSpace[] options) {
            this.options = options;
        }

        public List<GridCellComp> GetMoveOptions(GridControllerComp grid, GridSpace gridPosition) {
            List<GridCellComp> possibleMoves = new List<GridCellComp>();
            foreach (GridSpace moveOption in options) {
                GridCellComp cell = grid.GetCell(gridPosition.Add(moveOption));
                if (cell != null && !cell.ContainsOccupant(GridItemType.Hole)){
                    possibleMoves.Add(cell);
                }
            }
            return possibleMoves;
        }
    }
}
