﻿using Assets.LittleBunFooX2.Scripts.Audio;
using RSG;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.LittleBunFooX2.Scripts.UserInterace {

    public class ScoreScreenControllerComp : LBF2Behaviour {

        private Promise showPromise;
        [SerializeField] private Text scoreText;
        [SerializeField] private string scoreDescription;
        [SerializeField] private AudioClip scoreSound;
        [SerializeField] private GameObject ggScene;

        private int playerScore;

        public Text ScoreText {
            get { return scoreText; }
            set { scoreText = value; }
        }

        public string ScoreDescription {
            get { return scoreDescription; }
            set { scoreDescription = value; }
        }

        public AudioClip ScoreSound {
            get { return scoreSound; }
            set { scoreSound = value; }
        }
        
        public IPromise ShowScoreScreen(int score, GameObject ggScene) {
            this.ggScene = ggScene;
            ggScene.SetActive(true);
        	FindObjectOfType<AudioController>().PlayClip(ScoreSound);
            playerScore = score;
            showPromise = new Promise();
            scoreText.text = ScoreDescription + score;
            gameObject.SetActive(true);
            return showPromise;
        }

        public void OnRetryClicked() {
            ggScene.SetActive(false);
            gameObject.SetActive(false);
            showPromise.Resolve();
        }

        public void OnTweetClicked() {
            Application.OpenURL(@"https://twitter.com/intent/tweet/?text=I%20just%20bopped%20" + playerScore + "%20mice%20@mdm373&hashtags=shortnsweetjam,bunnyfoofoosmash");
        }

        public static ScoreScreenControllerComp Init(GameObject scoreScreenPrefab) {
            UserInterfaceController userInterface = FindObjectOfType<UserInterfaceController>();
            GameObject obj = Instantiate(scoreScreenPrefab, userInterface.transform);
            ScoreScreenControllerComp comp = obj.GetComponent<ScoreScreenControllerComp>();
            obj.SetActive(false);
            obj.transform.SetParent(userInterface.transform); 
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.pivot = Vector2.one * 0.5f;
            return comp;
        }
    }
}
