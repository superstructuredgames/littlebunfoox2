﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.LittleBunFooX2.Scripts.UserInterace {

    public class UserInterfaceController : LBF2Behaviour{

        [SerializeField] private Text chanceText;
        [SerializeField] private Text bopText;
        [SerializeField] private RectTransform uiRoot;
        [SerializeField] private Text cheeseText;
        [SerializeField] private Animator chanceAnimator;
        [SerializeField] private Animator bopAnimator;
        [SerializeField] private Animator cheeseAnimator;

        private int priorChanceCount;
        private int priorBopCount;
        private int priorCheeseCount;

        public Text ChanceText {
            get { return chanceText; }
            set { chanceText = value; }
        }

        public RectTransform UiRoot {
            get { return uiRoot; }
            set { uiRoot = value; }
        }

        public Text BopText {
            get { return bopText; }
            set { bopText = value; }
        }

        public Text CheeseText {
            get { return cheeseText; }
            set { cheeseText = value; }
        }

        public Animator ChanceAnimator {
            get { return chanceAnimator; }
            set { chanceAnimator = value; }
        }

        public Animator BopAnimator {
            get { return bopAnimator; }
            set { bopAnimator = value; }
        }

        public Animator CheeseAnimator {
            get { return cheeseAnimator; }
            set { cheeseAnimator = value; }
        }

        public void UpdateChanceDisplay(int count) {
            if (count != priorChanceCount) {
                chanceAnimator.SetTrigger("Emphasis");
                priorChanceCount = count;
            }
            chanceText.text = string.Format("Chances: {0}", count);
        }

        public void UpdateBopDisplay(int count) {
            if (count != priorBopCount) {
                bopAnimator.SetTrigger("Emphasis");
                priorBopCount = count;
            }
            bopText.text = string.Format("Bops: {0}", count);
        }

        public void UpdateCheeseCollection(int current, int total) {
            if (current == total && priorCheeseCount < total) {
                cheeseAnimator.SetTrigger("Important");
            } else if (priorCheeseCount == total && current < total) {
                cheeseAnimator.SetTrigger("Idle");
            } else if (current != priorCheeseCount) {
                cheeseAnimator.SetTrigger("Emphasis");
            }
            priorCheeseCount = current;
            cheeseText.text = current == total ? "Drop a\nPlate!" : string.Format("Cheeses\n{0} of {1}", current, total);
            
            
        }
        
    }
}
