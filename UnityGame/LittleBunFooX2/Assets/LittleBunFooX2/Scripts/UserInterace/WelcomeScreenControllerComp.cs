﻿using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.UserInterace {
    class WelcomeScreenControllerComp : LBF2Behaviour{
        private Promise confirmPromise;

        public static WelcomeScreenControllerComp Init(GameObject scoreScreenPrefab)
        {
            UserInterfaceController userInterface = FindObjectOfType<UserInterfaceController>();
            GameObject obj = Instantiate(scoreScreenPrefab, userInterface.transform);
            WelcomeScreenControllerComp comp = obj.GetComponent<WelcomeScreenControllerComp>();
            obj.SetActive(false);
            obj.transform.SetParent(userInterface.transform);
            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
            rectTransform.offsetMin = Vector3.zero;
            rectTransform.pivot = Vector2.one * 0.5f;
            return comp;
        }

        public IPromise ShowScreen() {
            gameObject.SetActive(true);
            confirmPromise = new Promise();
            return confirmPromise;
        }

        public void OnConfirmClicked() {
            gameObject.SetActive(false);
            confirmPromise.Resolve();
        }
    }
}
