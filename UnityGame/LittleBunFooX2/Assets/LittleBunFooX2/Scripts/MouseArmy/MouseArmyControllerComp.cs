﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.LittleBunFooX2.Scripts.BunnyChances;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using RSG;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.MouseArmy
{
    public class MouseArmyControllerComp : LBF2Behaviour{

        private GridItemControllerComp itemController;
        private MouseArmyControllerConfig config;
        private GridControllerComp grid;
        private BunnyChanceControllerComp chanceController;
        private readonly Dictionary<GridSpace, GridItemComp> escapingMice = new Dictionary<GridSpace, GridItemComp>();
        private static readonly GridItemType[] NONE = new GridItemType[0];
        [SerializeField] private AudioSource escapeSound;

        public AudioSource EscapeSound {
            get { return escapeSound; }
            set { escapeSound = value; }
        }

        public void Awake() {
            itemController = FindObjectOfType<GridItemControllerComp>();
            grid = FindObjectOfType<GridControllerComp>();
            chanceController = FindObjectOfType<BunnyChanceControllerComp>();
        }

        public void Init(MouseArmyControllerConfig armyConfig) {
            config = armyConfig;
        }

        public IPromise UpdateArmy() {
            return Promise.All(ProcessEscapedMice(), UpdateEscapingMice(), SpawnMiceAsNeeded());
        }

        private IPromise UpdateEscapingMice() {
            for (int y = 0; y < grid.GetGridSize().Y; y++){
                GridSpace cellPosition = new GridSpace(0, y);
                GridCellComp cell = grid.GetCell(cellPosition);
                FieldMouseComp escapedMouse = cell.GetOccupant<FieldMouseComp>(GridItemType.FieldMouse);
                if (escapedMouse != null && !escapedMouse.IsFat()) {
                    escapingMice.Add(cellPosition, escapedMouse);
                    
                }
            }
            return Promise.Resolved();
        }

        private IPromise ProcessEscapedMice() {
            List<IPromise> escapePromises = new List<IPromise>();
            foreach (KeyValuePair<GridSpace, GridItemComp> escapingMouse in escapingMice) {
                GridCellComp cell = grid.GetCell(escapingMouse.Key);
                FieldMouseComp mouse = cell.GetOccupant<FieldMouseComp>(GridItemType.FieldMouse);
                if (mouse == (FieldMouseComp)escapingMouse.Value && mouse != null) {
                    EscapeSound.Play();
                    cell.RemoveOccupant(escapingMouse.Value);
                    chanceController.RemoveChance();
                    escapePromises.Add(mouse.RunToHut().Then(() => itemController.DespawnClean(mouse)));
                }
            }
            escapingMice.Clear();
            return escapePromises.Any() ? Promise.All(escapePromises) : Promise.Resolved();
        }

        private IPromise SpawnMiceAsNeeded() {
            List<IPromise> promises = new List<IPromise>();
            int currentMouseCount = itemController.GetLiveCount(GridItemType.FieldMouse);
            if (currentMouseCount < config.MaxMice) {
                bool shouldSpawn = Random.Range(0.0f, 1.0f) < config.SpawnChance || currentMouseCount < config.MinMice;
                if (shouldSpawn) {
                    int minCol = grid.GetGridSize().X - config.SpawnWidth;
                    List<GridSpace> freeCells = GridCellAvailability.GetFreeCells(grid, minCol, grid.GetGridSize().X - 1, NONE);
                    if (freeCells.Count > 0) {
                        GridSpace spawnCell = freeCells[Random.Range(0, freeCells.Count)];
                        FieldMouseComp mouse;
                        promises.Add(itemController.Spawn(spawnCell, GridItemType.FieldMouse, out mouse));
                    }
                }
            }
            return Promise.All(promises);
        }

        
    }
}
