﻿using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.Audio;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.MouseArmy
{
    public class FieldMouseComp : BaseGridItemComp {

        private static readonly GridItemType[] HOLES_OR_CHEESE = { GridItemType.Hole, GridItemType.CheeseyBitz, GridItemType.CheeseyPlate  };
        private static readonly GridSpace[] ADJACENT = {
            new GridSpace(0, 1),
            new GridSpace(1, 0),
            new GridSpace(1, 1),
            new GridSpace(0, -1),
            new GridSpace(-1, 0),
            new GridSpace(-1, -1),
            new GridSpace(-1, 1),
            new GridSpace(1, -1),
        };

        [SerializeField] private AnimationCurve escapeCurve;
        [SerializeField] private float deathSpeed;
        [SerializeField] private AudioClip diveSound;
		[SerializeField] private AudioClip smashSound;
		[SerializeField] private AudioClip gobbleSound;
        [SerializeField] private AnimationCurve hutRunCurve;

        private GridItemPlacementManager placementManger;
        private GridItemControllerComp itemController;
        private AudioController audioController;
        [SerializeField] private int fatTime = 3;
        private int whenTurnedFat = -10;
        private bool isFat;
        


        public float DeathSpeed {
            get { return deathSpeed; }
            set { deathSpeed = value; }
        }

        public AnimationCurve EscapeCurve {
            get { return escapeCurve; }
            set { escapeCurve = value; }
        }

        public AudioClip DiveSound {
            get { return diveSound; }
            set { diveSound = value; }
        }

        public AudioClip SmashSound {
            get { return smashSound; }
            set { smashSound = value; }
        }

        public AudioClip GobbleSound {
            get { return gobbleSound; }
            set { gobbleSound = value; }
        }

        protected override void AwakeExtended() {
            placementManger = new GridItemPlacementManager(this, this);
            itemController = FindObjectOfType<GridItemControllerComp>();
            audioController = FindObjectOfType<AudioController>();
        }
        public override IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            return placementManger.Spawn(spawnGrid, spawnPosition);
        }

        public override IPromise Despawn(bool isCleanUp) {
            if (!isCleanUp) {
                GridCellComp escapeCell = null;
                foreach (GridSpace escapeOption in ADJACENT) {
                    GridCellComp cell = placementManger.GetCell(escapeOption);
                    if (cell != null && cell.ContainsOccupant(GridItemType.Bunny)) {
                        escapeCell = placementManger.GetCell(new GridSpace(-escapeOption.X, -escapeOption.Y));
                        break;
                    }
                }
                if (escapeCell != null && !escapeCell.HasAnyOccupant(HOLES_OR_CHEESE)) {
                    IPromise spawnPromise = Promise.Resolved();
                    if (!escapeCell.ContainsOccupant(GridItemType.Hole)) {
                        HoleGridItemComp hole;
                        spawnPromise = itemController.Spawn(escapeCell.GridPosition, GridItemType.Hole, out hole);
                    }
                    Promise escapePromise = new Promise();
                    StartCoroutine(EscapeToCell(escapeCell, escapePromise));
                    return escapePromise.Then(() => Promise.All(spawnPromise, placementManger.Despawn(0)));
                } else {
                	audioController.PlayClip(SmashSound);
                    animator.SetTrigger("Die");
                    return placementManger.Despawn(DeathSpeed);
                }
            } 
            return placementManger.Despawn(0);
        }

        private IEnumerator<YieldInstruction> EscapeToCell(GridCellComp escapeCell, IPendingPromise escapePromise) {
            animator.SetTrigger("Escape");
            audioController.PlayClip(DiveSound);
            float startTime = Time.time;
            float speed = escapeCurve[escapeCurve.length - 1].time;
            Vector3 startPosition = transform.position;
            while (Time.time - startTime < speed) {
                float elapsed = Time.time - startTime;
                float value = EscapeCurve.Evaluate(elapsed);
                transform.position = Vector3.Lerp(startPosition, escapeCell.GetAnchorPoint(), value);
                yield return new WaitForEndOfFrame();
            }
            transform.position = escapeCell.GetAnchorPoint();
            escapePromise.Resolve();
        }

        

        public override IPromise StepForward(out bool isBlocked, int stepCount) {
            Debug.Log((stepCount - whenTurnedFat <= fatTime) + " " + stepCount + " " + whenTurnedFat + " " + fatTime);
            IPromise promise =  stepCount - whenTurnedFat <= fatTime ? ProcessFat(out isBlocked, stepCount) : ProcessWalk(out isBlocked, stepCount);
            return promise;
        }

        private IPromise ProcessFat(out bool isBlocked, int stepCount) {
            isBlocked = true;
            if ((stepCount - whenTurnedFat) == fatTime && isFat) {
                isFat = false;
                animator.SetBool("Fat", false);
            }
            return Promise.Resolved();
        }

        private IPromise ProcessWalk(out bool isBlocked, int stepCount) {
            Debug.Log("walking " + stepCount);
            GridSpace direction = new GridSpace(-1, 0);
            GridCellComp cell = placementManger.GetCell(direction);
            isBlocked = true;
            List<IPromise> promises = new List<IPromise>();
            if (cell != null && !cell.HasAnyOccupant(HOLES_OR_CHEESE))
            {
                GridItemComp bitz = cell.GetOccupant(GridItemType.CheeseyBitz);
                GridItemComp plate = cell.GetOccupant(GridItemType.CheeseyPlate);
                if (bitz != null)
                {
                    audioController.PlayClip(GobbleSound);
                    animator.SetTrigger("Gobble");
                    cell.RemoveOccupant(bitz);
                    promises.Add(itemController.Despawn(bitz));
                }
                else if (plate != null)
                {
                    audioController.PlayClip(GobbleSound);
                    animator.SetTrigger("Gobble");
                    animator.SetBool("Fat", true);
                    isFat = true;
                    whenTurnedFat = stepCount;
                    cell.RemoveOccupant(plate);
                    promises.Add(itemController.Despawn(plate));
                }
                else
                {
                    animator.SetTrigger("Walk");
                }
                promises.Add(placementManger.StepForward(direction, MoveCurve, out isBlocked));

            }
            else
            {
                promises.Add(Promise.Resolved());
            }
            return Promise.All(promises);
        }

        public override GridCellComp GetCurrent() {
            return placementManger.GetCell(new GridSpace(0, 0));
        }

        public bool IsFat() {
            return isFat;
        }

        public IPromise RunToHut() {
            animator.SetTrigger("Walk");
            Promise runPomise = new Promise();
            StartCoroutine(RunToHut(runPomise));
            return runPomise;
        }

        private IEnumerator<YieldInstruction> RunToHut(IPendingPromise runPomise) {
            Vector3 start = transform.position;
            Vector3 desination = GameObject.FindGameObjectWithTag("MouseExitPoint").transform.position;
            float startTime = Time.time;
            float duration = hutRunCurve[hutRunCurve.length - 1].time;
            while (Time.time - startTime < duration) {
                float percent = hutRunCurve.Evaluate(Time.time - startTime);
                transform.position = Vector3.Lerp(start, desination, percent);
                transform.localScale = Vector3.one - Vector3.one*percent;
                yield return new WaitForEndOfFrame();
            }
            transform.position = desination;
            transform.localScale = Vector3.zero;
            runPomise.Resolve();
        }

        public AnimationCurve HutRunCurve {
            get { return hutRunCurve; }
            set { hutRunCurve = value; }
        }
    }
}
