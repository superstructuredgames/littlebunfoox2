﻿using System;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.MouseArmy {

    [Serializable]
    public class MouseArmyControllerConfig{
        [SerializeField] private int maxMice = 5;
        [SerializeField] private int minMice = 1;
        [SerializeField, Range(0, 1)]private float spawnChance = .66f;
        [SerializeField] private int spawnWidth = 2;
        
        public int MaxMice {
            get { return maxMice; }
            set { maxMice = value; }
        }

        public float SpawnChance {
            get { return spawnChance; }
            set { spawnChance = value; }
        }

        public int SpawnWidth {
            get { return spawnWidth; }
            set { spawnWidth = value; }
        }

        public int MinMice {
            get { return minMice; }
            set { minMice = value; }
        }
    }
}
