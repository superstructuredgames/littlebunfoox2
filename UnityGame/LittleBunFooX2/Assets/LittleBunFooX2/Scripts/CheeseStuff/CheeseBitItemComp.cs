﻿using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.CheeseStuff {
    public class CheeseBitItemComp : BaseGridItemComp
    {
        private GridItemPlacementManager placementManger;
        [SerializeField] private float despawnSpeed;

        protected override void AwakeExtended() {
            placementManger = new GridItemPlacementManager(this, this);
        }

        public override IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            return placementManger.Spawn(spawnGrid, spawnPosition);
        }

        public override IPromise Despawn(bool isCleanUp) {
            if (isCleanUp) {
                return Promise.Resolved();
            }
            animator.SetTrigger("Despawn");
            return placementManger.Despawn(DespawnSpeed);
        }

        public float DespawnSpeed {
            get { return despawnSpeed; }
            set { despawnSpeed = value; }
        }

        public override IPromise StepForward(out bool isBlocked, int currentStep) {
            isBlocked = false;
            return Promise.Resolved();
        }

        public override GridCellComp GetCurrent() {
            return placementManger.GetCell(new GridSpace(0, 0));
        }
        
    }
}
