﻿using System;
using System.Collections.Generic;
using Assets.LittleBunFooX2.Scripts.BunnyChances;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using Assets.LittleBunFooX2.Scripts.UserInterace;
using RSG;
using Random = UnityEngine.Random;

namespace Assets.LittleBunFooX2.Scripts.CheeseStuff {
    public class CheeseControllerComp : LBF2Behaviour{
        private GridItemControllerComp itemController;
        private GridControllerComp grid;
        private CheeseControllerConfig config;
        private static readonly GridItemType[] HOLES = {GridItemType.Hole};
        private UserInterfaceController ui;

        private int currentCollectCount;
        private BunnyChanceControllerComp chances;

        public void Awake() {
            itemController = FindObjectOfType<GridItemControllerComp>();
            grid = FindObjectOfType<GridControllerComp>();
            ui = FindObjectOfType<UserInterfaceController>();
            chances = FindObjectOfType<BunnyChanceControllerComp>();
        }

        public void Init(CheeseControllerConfig cheeseConfig) {
            config = cheeseConfig;
        }

        public IPromise UpdateFineCheeses() {
            if (chances.HasAnyChances()) {
                return Promise.All(SpawnCheeses(), PromptPlacement());
            } else {
                return Promise.Resolved();
            }
            
        }

        private IPromise PromptPlacement() {
            IPromise promise;
            if (currentCollectCount >= config.BitsToCollect) {
                List<GridCellComp> availCells = new List<GridCellComp>();
                for (int x = 0; x <= config.SpawnMaxCol; x++) {
                    for (int y = 0; y < grid.GetGridSize().Y; y++) {
                        GridSpace space = new GridSpace(x, y);
                        GridCellComp cell = grid.GetCell(space);
                        if (!cell.HasAnyOccupant(new[] {GridItemType.Hole})) {
                            availCells.Add(cell);
                            cell.AltHilight();
                        }
                    }
                }
                Promise pendingPromise = new Promise();
                promise = pendingPromise;
                RunCellSelectLoop(pendingPromise, availCells);
            } else {
                promise = Promise.Resolved();
            }
            return promise;
        }

        private void RunCellSelectLoop(IPendingPromise pendingPromise, List<GridCellComp> availCells) {
            grid.PlayerTapsCell().Then((cell) => {
                if (!cell.HasAnyOccupant(new[] {GridItemType.Hole})) {
                    currentCollectCount = 0;
                    GridItemComp spawned;
                    itemController.Spawn(cell.GridPosition, GridItemType.CheeseyPlate, out spawned);
                    foreach (GridCellComp availCell in availCells) {
                        availCell.AltUnHilight();
                    }
                    pendingPromise.Resolve();
                } else {
                    RunCellSelectLoop(pendingPromise, availCells);
                }
            });
        }

        private IPromise SpawnCheeses() {
            int currentBitsCount = itemController.GetLiveCount(GridItemType.CheeseyBitz);
            bool shouldSpawn = Random.Range(0.0f, 1.0f) < config.SpawnChance && currentBitsCount < config.MaxCheeseyBitz;
            if (shouldSpawn) {
                List<GridSpace> freeCells = GridCellAvailability.GetFreeCells(grid, config.SpawnMinCol,
                    config.SpawnMaxCol, HOLES);
                if (freeCells.Count > 0) {
                    GridSpace spawnCell = freeCells[Random.Range(0, freeCells.Count)];
                    GridItemComp cheese;
                    itemController.Spawn(spawnCell, GridItemType.CheeseyBitz, out cheese);
                }
            }
            return Promise.Resolved();
        }

        public void RegisterCheeseCollected() {
            currentCollectCount++;
            currentCollectCount = Math.Min(currentCollectCount, config.BitsToCollect);
        }

        public void Reset() {
            currentCollectCount = 0;
        }

        public void Update() {
            ui.UpdateCheeseCollection(currentCollectCount, config.BitsToCollect);
        }
        
    }
}
