﻿using System;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.CheeseStuff {

    [Serializable]
    public class CheeseControllerConfig{
        [SerializeField] private float spawnChance;
        [SerializeField] private int maxCheeseyBitz;
        [SerializeField] private int spawnMinCol;
		[SerializeField] private int spawnMaxCol;
        [SerializeField] private int bitsToCollect;

        public float SpawnChance {
            get { return spawnChance; }
            set { spawnChance = value; }
        }

        public int MaxCheeseyBitz {
            get { return maxCheeseyBitz; }
            set { maxCheeseyBitz = value; }
        }

        public int SpawnMinCol {
			get { return spawnMinCol; }
			set { spawnMinCol = value; }
        }


        public int SpawnMaxCol {
			get { return spawnMaxCol; }
			set { spawnMaxCol = value; }
        }

        public int BitsToCollect {
            get { return bitsToCollect; }
            set { bitsToCollect = value; }
        }
    }
}
