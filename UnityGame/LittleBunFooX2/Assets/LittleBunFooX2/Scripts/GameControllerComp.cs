﻿using Assets.LittleBunFooX2.Scripts.Audio;
using Assets.LittleBunFooX2.Scripts.BunnyChances;
using Assets.LittleBunFooX2.Scripts.BunnyControl;
using Assets.LittleBunFooX2.Scripts.CheeseStuff;
using Assets.LittleBunFooX2.Scripts.Grid;
using Assets.LittleBunFooX2.Scripts.GridItems;
using Assets.LittleBunFooX2.Scripts.MouseArmy;
using Assets.LittleBunFooX2.Scripts.UserInterace;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts {
    public class GameControllerComp : LBF2Behaviour{

        [SerializeField] private GridControllerConfig gridConfig;
        [SerializeField] private GridItemControllerConfig itemConfig;
        [SerializeField] private MouseArmyControllerConfig mouseArmyConfig;
        [SerializeField] private BunnyChanceControllerConfig chanceConfig;
        [SerializeField] private BunnyControllerConfig bunnyConfig;
        [SerializeField] private GameObject scoreScreenPrefab;
        [SerializeField] private GameObject welcomeScreenPrefab;
        [SerializeField] private CheeseControllerConfig cheeseConfig;
        [SerializeField] private GameObject ggScene;

        public GridControllerConfig GridConfig {
            get { return gridConfig; }
            set { gridConfig = value; }
        }

        public GridItemControllerConfig ItemConfig {
            get { return itemConfig; }
            set { itemConfig = value; }
        }


        public BunnyChanceControllerConfig ChanceConfig {
            get { return chanceConfig; }
            set { chanceConfig = value; }
        }

        public MouseArmyControllerConfig MouseArmyConfig {
            get { return mouseArmyConfig; }
            set { mouseArmyConfig = value; }
        }

        public BunnyControllerConfig BunnyConfig {
            get { return bunnyConfig; }
            set { bunnyConfig = value; }
        }

        public GameObject ScoreScreenPrefab {
            get { return scoreScreenPrefab; }
            set { scoreScreenPrefab = value; }
        }

        public GameObject WelcomeScreenPrefab {
            get { return welcomeScreenPrefab; }
            set { welcomeScreenPrefab = value; }
        }

        public void Start() {
            ScoreScreenControllerComp scoreScreen = ScoreScreenControllerComp.Init(scoreScreenPrefab);
            WelcomeScreenControllerComp welcomeScreen = WelcomeScreenControllerComp.Init(welcomeScreenPrefab);

            BunnyChanceControllerComp chanceController = FindObjectOfType<BunnyChanceControllerComp>();
            GridControllerComp grid = FindObjectOfType<GridControllerComp>();
            GridItemControllerComp itemController = FindObjectOfType<GridItemControllerComp>();
            BunnyControllerComp bunnyControllerComp = FindObjectOfType<BunnyControllerComp>();
            CheeseControllerComp cheeseController = FindObjectOfType<CheeseControllerComp>();
            grid.Generate(gridConfig);
            MouseArmyControllerComp mouseArmy = FindObjectOfType<MouseArmyControllerComp>();

            itemController.Init(ItemConfig);
            mouseArmy.Init(MouseArmyConfig);
            chanceController.Init(ChanceConfig);
            bunnyControllerComp.Init(BunnyConfig);
            cheeseController.Init(CheeseConfig);
            welcomeScreen.ShowScreen()
            	.Then(() => bunnyControllerComp.SpawnBunny())
                .Then(() => RunGameLoop(bunnyControllerComp, mouseArmy, chanceController, scoreScreen, itemController, cheeseController, ggScene))
                .Catch(Debug.LogError);



        }
        
        public CheeseControllerConfig CheeseConfig {
            get { return cheeseConfig; }
            set { cheeseConfig = value; }
        }

        public GameObject GgScene {
            get { return ggScene; }
            set { ggScene = value; }
        }

        private static IPromise ResetState(GridItemControllerComp itemController, BunnyChanceControllerComp chanceController, BunnyControllerComp bunnyController, CheeseControllerComp cheeseController) {
            itemController.RemoveAll();
            chanceController.Reset();
            cheeseController.Reset();
            return bunnyController.SpawnBunny();
        }
        
        private static IPromise RunGameLoop(BunnyControllerComp bunnyController, MouseArmyControllerComp mouseArmy, BunnyChanceControllerComp chanceController, ScoreScreenControllerComp scoreScreen, GridItemControllerComp itemController, CheeseControllerComp cheeseController, GameObject ggScene) {
            return bunnyController.PlayerMoves()
                .Then( () => itemController.StepForward())
                .Then( () => mouseArmy.UpdateArmy())
                .Then( () => cheeseController.UpdateFineCheeses())
                .Then( () => {
                    if (chanceController.HasAnyChances()) {
                        return RunGameLoop(bunnyController, mouseArmy, chanceController, scoreScreen, itemController, cheeseController, ggScene);
                    }
                    int boopCount = bunnyController.GetBoopCount();
                    return scoreScreen.ShowScoreScreen(boopCount, ggScene)
                        .Then(() => ResetState(itemController, chanceController, bunnyController, cheeseController))
                        .Then(() => RunGameLoop(bunnyController, mouseArmy, chanceController, scoreScreen, itemController, cheeseController, ggScene));
                });
        }
        
    }
}
