﻿using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.Audio {
    public class AudioController : MonoBehaviour {

	

        public void PlayClip (AudioClip toPlay) {
            AudioSource.PlayClipAtPoint(toPlay, Camera.main.transform.position);
        }
    }
}

