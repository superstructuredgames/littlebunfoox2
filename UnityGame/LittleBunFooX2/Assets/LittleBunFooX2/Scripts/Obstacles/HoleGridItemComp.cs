﻿using Assets.LittleBunFooX2.Scripts.Grid;
using RSG;
using UnityEngine;

namespace Assets.LittleBunFooX2.Scripts.GridItems {

    public class HoleGridItemComp : BaseGridItemComp {

        [SerializeField] private int lifeTime;
        private GridItemPlacementManager placementManager;
        private GridItemControllerComp itemController;
        private string[] triggers = new[] {"medium", "small", "despawn"};
        private int startLifetime;
        private int lastStep = -1;

        public int LifeTime {
            get { return lifeTime; }
            set { lifeTime = value; }
        }


        public override IPromise Spawn(GridControllerComp spawnGrid, GridSpace spawnPosition) {
            startLifetime = lifeTime;
            return placementManager.Spawn(spawnGrid, spawnPosition);
        }

        public override IPromise Despawn(bool isCleanUp) {
            Promise promise = new Promise();
            StartCoroutine(placementManager.Wait(promise, SpawnSpeed));
            return promise;
        }

        public override IPromise StepForward(out bool isBlocked, int currentStep) {
            isBlocked = false;
            animator.SetTrigger(triggers[startLifetime - lifeTime]);
            if (currentStep != lastStep) {
                lifeTime--;
            }
            if (lifeTime == 0) {
                GetCurrent().RemoveOccupant(this);
                return itemController.Despawn(this);
            }
            Promise promise = new Promise();
            StartCoroutine(placementManager.Wait(promise, SpawnSpeed));
            lastStep = currentStep;
            return promise;
        }

        public override GridCellComp GetCurrent() {
            return placementManager.GetCell(new GridSpace(0, 0));
        }

        protected override void AwakeExtended() {
            placementManager = new GridItemPlacementManager(this, this);
            itemController = FindObjectOfType<GridItemControllerComp>();
        }
    }
}
